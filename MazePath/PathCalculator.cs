﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MazePath
{
    public class PathCalculator
    {
        /// <summary>
        /// Calculates a path through the specified maze. Employs a heuristic
        /// approach to determine the route to the path, based on the A* pathfinding
        /// algorithm. 
        /// </summary>
        /// <param name="maze">The maze to calculate a path through</param>
        /// <returns>A list of Coordinate values which show the steps through 
        /// the maze from the start point to the end point</returns>
        public List<Coordinate> CalculatePath(Maze maze)
        {
            var openList = new List<Coordinate>();
            var closedList = new List<Coordinate>();
            Coordinate currentLocation = maze.StartPoint;

            openList.Add(currentLocation);

            while (!ListContainsValue(closedList, maze.EndPoint))
            {
                if (openList.Count == 0)
                {
                    //There are no more available paths to explore, so no route exists
                    return null;                    
                }

                openList = openList.OrderBy(x => x.F).ToList();
                                
                currentLocation = openList.First();
                closedList.Add(currentLocation);
                openList.RemoveAt(0);
                
                try
                {
                    //Check above
                    if (currentLocation.iVal - 1 >= 0)
                    {
                        if (MoveIsValid(maze, openList, closedList, currentLocation.iVal - 1, currentLocation.jVal))
                        {
                            openList.Add(CreateCoordinateWithScore(
                                maze.EndPoint, currentLocation, currentLocation.iVal - 1, currentLocation.jVal));
                        }
                    }
                    //Check below
                    if (currentLocation.iVal + 1 <= maze.Height)
                    {
                        if (MoveIsValid(maze, openList, closedList, currentLocation.iVal + 1, currentLocation.jVal))
                        {
                            openList.Add(CreateCoordinateWithScore(
                                maze.EndPoint, currentLocation, currentLocation.iVal + 1, currentLocation.jVal));
                        }
                    }
                    //Check left
                    if (currentLocation.jVal - 1 >= 0)
                    {
                        if (MoveIsValid(maze, openList, closedList, currentLocation.iVal, currentLocation.jVal - 1))
                        {
                            openList.Add(CreateCoordinateWithScore(
                                maze.EndPoint, currentLocation, currentLocation.iVal, currentLocation.jVal - 1));
                        }
                    }
                    //Check right
                    if (currentLocation.jVal + 1 <= maze.Width)
                    {
                        if (MoveIsValid(maze, openList, closedList, currentLocation.iVal, currentLocation.jVal + 1))
                        {
                            openList.Add(CreateCoordinateWithScore(
                                maze.EndPoint, currentLocation, currentLocation.iVal, currentLocation.jVal + 1));
                        }
                    }
                }
                catch (IndexOutOfRangeException ex) //ToDo: Proper exception handling
                {
                    Console.WriteLine(
                        "An error was encountered whilst navigating through the file. "+
                        "Please ensure the file is correctly formatted. Exception message: {0}", ex.Message);
                    return null;
                }
            }

            var foundPath = new List<Coordinate>();
            var lastCoordinate = closedList.Last();
            
            while (maze.StartPoint.iVal != lastCoordinate.iVal || maze.StartPoint.jVal != lastCoordinate.jVal)
            {
                foundPath.Add(lastCoordinate);
                lastCoordinate = lastCoordinate.Parent;
            }
            foundPath.Add(maze.StartPoint);  
            return foundPath;
        }

        /// <summary>
        /// Ensure the move to the surrounding tile would be valid.
        /// </summary>
        /// <param name="maze">The maze being navigated</param>
        /// <param name="openList">The current list of already approved moves</param>
        /// <param name="iVal">The i value of the next move</param>
        /// <param name="jVal">The j value of the next move</param>
        /// <returns></returns>
        private bool MoveIsValid(Maze maze, List<Coordinate> openList, List<Coordinate> closedList, int iVal, int jVal)
        {
            if (!maze.Map[iVal, jVal])
            {
                return false;
            }
            
            var existsInClose = closedList.FirstOrDefault(c => c.iVal == iVal && c.jVal == jVal);
            if (existsInClose != null)
            {
                return false;
            }
         
            var existsInOpen = openList.FirstOrDefault(c => c.iVal == iVal && c.jVal == jVal);
            if (existsInOpen != null)
            {                
                return false;
            }
            return true;
        }

        /// <summary>
        /// Constructs a new Coordinate object, populating its co-ordinate position
        /// and calculating its heuristic scoring relative to the target co-ordinate.
        /// </summary>
        /// <param name="targetCoord">The co-ordinate maze target</param>
        /// <param name="currentCoord">The current co-ordinate being processed</param>
        /// <param name="iVal">The i value relative to the maze map of the new co-ordinate</param>
        /// <param name="jVal">The j value relative to the maze map of the new co-ordinate</param>
        /// <returns></returns>
        private Coordinate CreateCoordinateWithScore(Coordinate targetCoord, Coordinate currentCoord, int iVal, int jVal)
        {
            var coord = new Coordinate(iVal, jVal);
            int distX = iVal - targetCoord.iVal;
            int distY = jVal - targetCoord.jVal;
            coord.H = System.Math.Abs(distX) + System.Math.Abs(distY);
            coord.F = coord.H + 1;
            coord.Parent = currentCoord;
            return coord;
        }

        /// <summary>
        /// Determine whether the target co-ordinate exists in the specified list
        /// by comparing values.
        /// </summary>
        /// <param name="list">The list of co-ordinates to check</param>
        /// <param name="targetCoord">The target co-ordinate to locate</param>
        /// <returns>True if it exists in the list, false otherwise</returns>
        private bool ListContainsValue(List<Coordinate> list, Coordinate targetCoord)
        {
            foreach (var coord in list)
            {
                if (coord.iVal == targetCoord.iVal && coord.jVal == targetCoord.jVal)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
