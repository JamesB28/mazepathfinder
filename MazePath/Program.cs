﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MazePath
{
    public class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 1)
            {
                Console.WriteLine("No maze file has been specified! Include a full valid file path to a maze file to calculate a route.");
            }
            else
            {
                var fileHandler = new FileHandler();
                var pathCalculator = new PathCalculator();
                var pathWriter = new PathWriter();

                var maze = fileHandler.ReadInFile(args[0]);
                if (maze != null)
                {
                    var mazePath = pathCalculator.CalculatePath(maze);
                    pathWriter.WriteSolutionToConsole(maze, mazePath);
                }
            }    
        }        
    }
}
