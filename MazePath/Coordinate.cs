﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MazePath
{    
    public class Coordinate
    {
        public int iVal { get; set; }
        public int jVal { get; set; }        
        public Coordinate Parent { get; set; }        

        // Below used in pathfinding only

        /// <summary>
        /// weighted distance to target
        /// </summary>
        public int F { get; set; } 

        /// <summary>
        /// Heuristic distance to target
        /// </summary>
        public int H { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public Coordinate() { }

        /// <summary>
        /// Instantiate a new Co-ordinate which represents a location point
        /// within a maze.
        /// </summary>
        /// <param name="rowPos">The x across index position from 0</param>
        /// <param name="columnPos">The y down index position starting from 0</param>
        public Coordinate(int columnPos, int rowPos)
        {
            iVal = columnPos;
            jVal = rowPos;
            F = 0;
            H = 0;
        }
    }
}
