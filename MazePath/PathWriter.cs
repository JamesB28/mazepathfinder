﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MazePath
{
    public class PathWriter
    {
        /// <summary>
        /// Writes the specified maze to the console, drawing each of the path
        /// co-ordinate steps into the output.
        /// </summary>
        /// <param name="maze">The maze to display on the console</param>
        /// <param name="mazePath">The Co-ordinate steps from the start point 
        /// to the end point</param>
        public void WriteSolutionToConsole(Maze maze, List<Coordinate> mazePath)
        {
            if (mazePath == null)
            {   
                Console.WriteLine("No path could be found through the maze.");
                return;
            }

            var consoleLines = new char[maze.Height, maze.Width];

            for (int i = 0; i < maze.Height; i++)
            {
                for (int j = 0; j < maze.Width; j++)
                {                    
                    if (!maze.Map[i, j])
                    {
                        consoleLines[i, j] = '#';
                    }
                    else
                    {
                        consoleLines[i, j] = ' ';
                    }
                }
            }            

            foreach (var coord in mazePath)
            {
                consoleLines[coord.iVal, coord.jVal] = 'X';
            }

            consoleLines[maze.StartPoint.iVal, maze.StartPoint.jVal] = 'S';
            consoleLines[maze.EndPoint.iVal, maze.EndPoint.jVal] = 'E';

            for (int i = 0; i < maze.Height; i++)
            {
                for (int j = 0; j < maze.Width; j++)
                {
                    Console.Write(consoleLines[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
