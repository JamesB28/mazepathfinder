﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace MazePath
{
    /// <summary>
    /// This class is responsible for retrieving values from the specified input
    /// file and returning a populated system Maze object.
    /// </summary>
    public class FileHandler
    {
        /// <summary>
        /// Reads in the file at the specified URI, populating a new Maze object
        /// using the properties defined within the file.
        /// </summary>
        /// <param name="fileUri">The full URI of the maze file to read in</param>
        /// <returns>A populated Maze object if the file was correctly parsed, null otherwise</returns>
        public Maze ReadInFile(string fileUri) 
        {
            try
            {
                string[] lines = File.ReadAllLines(fileUri);

                var heightWidth = ReadFirstTwoLineValues(lines[0]);
                var startiStartj = ReadFirstTwoLineValues(lines[1]);
                var endiEndj = ReadFirstTwoLineValues(lines[2]);

                var maze = new bool[heightWidth.Item1, heightWidth.Item2];
                for (int i = 0; i < heightWidth.Item1; i++)
                {
                    AddRowToMaze(maze, i, lines[i + 3]);
                }
                
                return
                    new Maze(maze,
                        new Coordinate(startiStartj.Item1, startiStartj.Item2),
                        new Coordinate(endiEndj.Item1, endiEndj.Item2));
            }
            catch (FormatException)
            {
                Console.WriteLine(
                    "A non-numeric value was located in the file at URI: {0}.", fileUri);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine(
                    "The file at URI: {0} could not be found.", fileUri);
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "An exception was encountered whilst parsing the file at URI: {0}. Exception details: {1}", 
                    fileUri, ex);
            }
            return null;
        }

        /// <summary>
        /// Retrieves the first two space delimited integer values from the specified
        /// string and returns them in a Tuple.
        /// </summary>
        /// <param name="fileLine">The string file line to read values from</param>
        /// <returns>A Tuple where Item1 is the first item read and Item2 is the second.</returns>
        private Tuple<int, int> ReadFirstTwoLineValues(string fileLine)
        {
            var heightWidth = fileLine.Split((string[])null, StringSplitOptions.None);

            return new Tuple<int, int>(int.Parse(heightWidth[0]), int.Parse(heightWidth[1]));
        }

        /// <summary>
        /// Parse and add values from the file line passed in to the passed in 
        /// maze for the row as specified by index.
        /// </summary>
        /// <param name="mazeMap">The current maze map</param>
        /// <param name="rowIndex">The index of the maze map to populate</param>
        /// <param name="fileLine">The line of the file to parse values from</param>
        private void AddRowToMaze(bool[,] mazeMap, int rowIndex, string fileLine)
        {
            var rowValues = fileLine.Split((string[])null, StringSplitOptions.None);
            for (int i = 0; i < rowValues.Length; i++)
            {
                mazeMap[rowIndex, i] = int.Parse(rowValues[i]) == 0;
            }
        }
    }
}
