﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MazePath
{
    public class Maze
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public Coordinate StartPoint { get; set; }
        public Coordinate EndPoint { get; set; }
        public bool[,] Map { get; set; } 

        /// <summary>
        /// Construct a new Maze object with a 2D map of booleans, a start point
        /// and an end point. 
        /// </summary>
        /// <param name="map">
        /// 2D array of bool values, where true is passable, false is impassable.
        /// formatted as such [RowNumber, ItemsPerRow]
        /// </param>
        /// <param name="start">A coordinate representing the start position on 
        /// the maze on the map</param>
        /// <param name="end">A coordinate representing the start position on the 
        /// maze on the map</param>
        public Maze(bool[,] map, Coordinate start, Coordinate end)
        {
            Map = map;
            Width = map.GetLength(1);
            Height = map.GetLength(0);
            StartPoint = start;
            EndPoint = end;
        }
    }
}
